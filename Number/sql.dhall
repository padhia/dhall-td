let nonum = (../Prelude.dhall).Optional.null Natural

let shownum =
      λ(x : Optional Natural) → Optional/fold Natural x Text Natural/show "*"

let showpair =
        λ(_1 : Optional Natural)
      → λ(_2 : Optional Natural)
      →       if nonum _1 && nonum _2
        
        then  ""
        
        else  if nonum _2
        
        then  "(${shownum _1})"
        
        else  "(${shownum _1},${shownum _2})"

let sql = λ(l : ./Type.dhall) → "NUMBER${showpair l.prec l.scale}"

let example0 = assert : sql { prec = Some 18, scale = Some 3 } ≡ "NUMBER(18,3)"

let example1 =
      assert : sql { prec = Some 18, scale = None Natural } ≡ "NUMBER(18)"

let example2 =
      assert : sql { prec = None Natural, scale = Some 3 } ≡ "NUMBER(*,3)"

let example3 =
      assert : sql { prec = None Natural, scale = None Natural } ≡ "NUMBER"

in  sql
