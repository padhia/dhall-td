  λ(x : ./Type.dhall)
→     x.table
  ||  x.view
  ||  x.macro
  ||  x.trigger
  ||  x.role
  ||  x.database
  ||  x.user
  ||  x.proc
  ||  x.zone
  ||  x.owner_proc
  ||  x.ext_proc
