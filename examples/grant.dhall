let TD = ../package.dhall

let tvm = TD.DDLPrivileges::{ table = True, view = True, macro = True }

let example =
      TD.Grant.sql
        TD.Grant::{
        , priv = TD.Privileges::{ select = True, create = tvm, drop = tvm }
        , to = [ "DBA_ROLE" ]
        , on = TD.Grant.Target.database "CUSTOMERS"
        , with_grant = True
        }

in  example
