let TD = ../package.dhall

let Col = TD.Column

let ColType = TD.ColumnType.Type

let c =
      { _1 = Col::{ name = "AGENCY_KEY", type = ColType.Integer }
      , _2 = Col::{ name = "SRC_SYSTEM_KEY", type = ColType.Integer }
      , _3 = Col::{ name = "CUST_NBR", type = ColType.Varchar 12 }
      , _4 = Col::{ name = "AGCY_NM", type = ColType.Varchar 100 }
      , _5 =
          Col::{
          , name = "AGCY_OWN_NM"
          , type = ColType.Varchar 50
          , null = True
          , compress = Some ([] : List Text)
          }
      , _6 =
          Col::{
          , name = "SRC_DEL_FLG"
          , type = ColType.Char 1
          , default = Some "'Y'"
          , compress = Some [ "'Y'" ]
          }
      , _7 =
          Col::{
          , name = "CRE_TS"
          , type = ColType.Timestamp 0
          , default = Some "CURRENT_TIMESTAMP(0)"
          }
      , _8 =
          Col::{
          , name = "UPDT_TS"
          , type = ColType.Timestamp 0
          , default = Some "CURRENT_TIMESTAMP(0)"
          }
      , _9 = Col::{ name = "ETL_PRCS_DT", type = ColType.Date }
      }

let tbl =
      TD.Table::{
      , sch = "CUST_DB"
      , name = "AGENCY"
      , columns = [ c._1, c._2, c._3, c._4, c._5, c._6, c._7, c._8, c._9 ]
      , indexes = [ TD.Index::{ columns = [ c._1 ], primary = True } ]
      }

in  TD.Table.sql tbl
