let TU = ./Type.dhall

let sql =
        λ(x : TU)
      → λ(len : Optional Natural)
      → let onum = λ(f : Natural → Text) → Optional/fold Natural len Text f ""

        let num1 = onum (λ(n : Natural) → "(${Natural/show n})")

        let num2 =
                λ(x : Natural)
              → "(${onum (λ(n : Natural) → "${Natural/show n},")}${Natural/show
                                                                     x})"

        in  merge
              { Year = "YEAR" ++ num1
              , Month = "MONTH" ++ num1
              , Day = "DAY" ++ num1
              , Hour = "HOUR" ++ num1
              , Minute = "MINUTE" ++ num1
              , Second = λ(x : Natural) → "SECOND" ++ num2 x
              }
              x

let example0 = assert : sql TU.Hour (None Natural) ≡ "HOUR"

let example1 = assert : sql TU.Hour (Some 4) ≡ "HOUR(4)"

let example2 = assert : sql (TU.Second 0) (None Natural) ≡ "SECOND(0)"

let example3 = assert : sql (TU.Second 6) (Some 4) ≡ "SECOND(4,6)"

in  sql
