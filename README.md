# Dhall Teradata

Dhall bindings to encode some of the commonly used [Teradata](https://docs.teradata.com) *DCL* and *DDL* information.

## Why?

SQL is a very powerful language and has its place for writing complex data retrieval queries. However, a subset of SQL, mainly DCL (*Data Control Language*) and DDL (*Data Definition Language*), which are primarily used for describing database authorization information and schema objects definitions respectively, don't use nor require SQL's powerful data query capabilities (with exception of View definitions).

[dhall](https://github.com/dhall-lang/dhall-lang), on the other hand, is a strictly typed but a very flexible configuration/templating language with ability to represent information succinctly and without repetition.

This project is an experiment to use *dhall*, instead of DCL and DDL, to record information and generate SQL statements using dhall's templating capabilities.

Like any complex DBMS, Teradata is highly configurable and supports a slew of options. For example, per the official documentation, *Table Syntax* and description of all supported options take up nearly 200 pages. This project, which is WIP, follows (a highly subjective) 80-20 rule; meaning it supports a small fraction of all possible options, with the hope that they be sufficient in the most common use-cases.
