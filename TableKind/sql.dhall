  λ(x : ./Type.dhall)
→ merge
    { table = "TABLE"
    , view = "VIEW"
    , macro = "MACRO"
    , proc = "PROCEDURE"
    , ji = "JOIN INDEX"
    }
    x
