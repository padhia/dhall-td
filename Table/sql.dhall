let Column = ../Column/Type.dhall

let Column/sql = ../Column/sql.dhall

let Index = ../Index/Type.dhall

let Index/sql = ../Index/sql.dhall

let List/null = (../Prelude.dhall).List.null

let mklist =
        λ(a : Type)
      → λ(f : a → Text)
      → λ(xs : List a)
      → let csl = (../Prelude.dhall).Text.concatSep "\n, "

        let List/map = (../Prelude.dhall).List.map

        in  csl (List/map a Text f xs)

in    λ(t : ./Type.dhall)
    → let set = if t.multiset then "MULTISET" else "SET"

      let coldefs = mklist Column Column/sql t.columns

      let ixdefs = mklist Index Index/sql t.indexes

      in  ''
          CREATE ${set} TABLE ${./show.dhall t}
          ( ${coldefs}
          ) ${if List/null Index t.indexes then "NO PRIMARY INDEX" else ixdefs}
          ''
