< BLOB : Natural
| Byte : Natural
| Char : Natural
| CLOB : Natural
| Date
| Decimal : ../Decimal/Type.dhall
| Float
| Byteint
| Smallint
| Integer
| Bigint
| Interval : ../Interval/Type.dhall
| Json : Natural
| Number : ../Number/Type.dhall
| Time : Natural
| Timestamp : Natural
| Varbyte : Natural
| Varchar : Natural
| XML : Natural
>
