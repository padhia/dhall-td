let ColType = ./Type.dhall

let vartype = λ(t : Text) → λ(l : Natural) → "${t}(${Natural/show l})"

let handlers =
      { BLOB = vartype "BLOB"
      , Byte = vartype "BYTE"
      , Char = vartype "CHAR"
      , CLOB = vartype "CLOB"
      , Date = "DATE"
      , Decimal = ../Decimal/sql.dhall
      , Float = "REAL"
      , Byteint = "BYTEINT"
      , Smallint = "SMALLINT"
      , Integer = "INTEGER"
      , Bigint = "BIGINT"
      , Interval = ../Interval/sql.dhall
      , Json = vartype "JSON"
      , Number = ../Number/sql.dhall
      , Time = vartype "TIME"
      , Timestamp = vartype "TIMESTAMP"
      , Varbyte = vartype "VARBYTE"
      , Varchar = vartype "VARCHAR"
      , XML = vartype "XML"
      }

let sql = λ(x : ColType) → merge handlers x

let example0 = assert : sql (ColType.Char 6) ≡ "CHAR(6)"

in  sql
