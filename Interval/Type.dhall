let TemporalUnit = ../TemporalUnit/Type.dhall

in  { of : TemporalUnit, len : Natural, to : Optional TemporalUnit }
