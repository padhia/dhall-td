let Interval = ./Type.dhall

let TU = ../TemporalUnit/Type.dhall

let TU/sql = ../TemporalUnit/sql.dhall

let sql =
        λ(x : Interval)
      →     "INTERVAL "
        ++  TU/sql x.of (Some x.len)
        ++  Optional/fold
              TU
              x.to
              Text
              (λ(y : TU) → " TO ${TU/sql y (None Natural)}")
              ""

let example0 =
      assert : sql { of = TU.Year, len = 3, to = None TU } ≡ "INTERVAL YEAR(3)"

let example1 =
        assert
      :   sql { of = TU.Day, len = 4, to = Some (TU.Second 6) }
        ≡ "INTERVAL DAY(4) TO SECOND(6)"

in  sql
