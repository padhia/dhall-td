let defval =
        λ(x : Optional Text)
      → Optional/fold Text x Text (λ(y : Text) → " DEFAULT " ++ y) ""

let cmprs =
        λ(xs : Optional (List Text))
      → let cst = (../Prelude.dhall).Text.concatSep ", "

        let List/null = (../Prelude.dhall).List.null

        let mklist =
                λ(xs : List Text)
              → if List/null Text xs then "" else " (${cst xs})"

        in  Optional/fold
              (List Text)
              xs
              Text
              (λ(y : List Text) → " COMPRESS" ++ mklist y)
              ""

let sql =
        λ(c : ./Type.dhall)
      →     "${c.name} ${../ColumnType/sql.dhall c.type}"
        ++  (if c.null then "" else " NOT NULL")
        ++  defval c.default
        ++  cmprs c.compress

let example0 =
      let col =
              ./default.dhall
            ⫽ { name = "c1"
              , type = (../ColumnType/Type.dhall).Char 5
              , compress = Some [ "'A'", "'B'" ]
              }

      in  assert : sql col ≡ "c1 CHAR(5) NOT NULL COMPRESS ('A', 'B')"

let example1 =
      let col =
              ./default.dhall
            ⫽ { name = "c1"
              , type = (../ColumnType/Type.dhall).Char 5
              , compress = Some ([] : List Text)
              }

      in  assert : sql col ≡ "c1 CHAR(5) NOT NULL COMPRESS"

let example2 =
      let col =
              ./default.dhall
            ⫽ { name = "c1"
              , type = (../ColumnType/Type.dhall).Char 5
              , null = True
              }

      in  assert : sql col ≡ "c1 CHAR(5)"

let example3 =
      let col =
              ./default.dhall
            ⫽ { name = "c1"
              , type = (../ColumnType/Type.dhall).Varchar 128
              , default = Some "USER"
              }

      in  assert : sql col ≡ "c1 VARCHAR(128) NOT NULL DEFAULT USER"

in  sql
