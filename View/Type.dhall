let Table = ../Table/Type.dhall

let View
    : Type
    =   ∀(View : Type)
      → ∀ ( MakeView
          :   { sch : Text
              , name : Text
              , depends : List < view : View | table : Table >
              }
            → View
          )
      → View

in  View
