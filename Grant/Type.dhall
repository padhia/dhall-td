{ type : < grant | revoke >
, except : Bool
, priv : ../Privileges/Type.dhall
, to : List Text
, on : ./Target.dhall
, with_grant : Bool
}
