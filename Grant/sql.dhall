let sql =
        λ(p : ./Type.dhall)
      → let command = merge { grant = "GRANT", revoke = "REVOKE" } p.type

        let priv = ./priv_sql.dhall p.priv p.except

        let with_gopt = if p.with_grant then " WITH GRANT OPTION" else ""

        let object =
              merge
                { database = λ(x : Text) → x
                , tvm = λ(x : ../TVM/Type.dhall) → ../TVM/sql.dhall x
                }
                p.on

        let grantee = (../Prelude.dhall).Text.concatSep ", " p.to

        in  "${command} ${priv} ON ${object} TO ${grantee}${with_gopt}"

in  sql
