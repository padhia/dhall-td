let sql =
        λ(p : ../Privileges/Type.dhall)
      → λ(x : Bool)
      → let y = ../Privileges/any.dhall p

        let all = if x then [ "ALL" ] else [] : List Text

        let but = if x && y then [ "BUT" ] else [] : List Text

        let privs =
                    if y

              then  [ ../Privileges/sql.dhall p ]

              else  if x

              then  [] : List Text

              else  [ "SHOW" ]

        in  (../Prelude.dhall).Text.concatSep " " (all # but # privs)

let none = ../Privileges/default.dhall

let some = none ⫽ { select = True, insert = True }

let example0 = assert : sql some False ≡ "SELECT, INSERT"

let example1 = assert : sql some True ≡ "ALL BUT SELECT, INSERT"

let example2 = assert : sql none True ≡ "ALL"

let example3 = assert : sql none False ≡ "SHOW"

in  sql
