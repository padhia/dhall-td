{ type = < grant | revoke >.grant
, except = False
, priv = ../Privileges/default.dhall
, to = [ env:USER ? "PUBLIC" ]
, on = (./Target.dhall).database "DBC"
, with_grant = False
}
