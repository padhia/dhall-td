let Text/concatSep = (../Prelude.dhall).Text.concatSep

let List/map = (../Prelude.dhall).List.map

let Column = ../Column/Type.dhall

let collist =
        λ(xs : List Column)
      → Text/concatSep ", " (List/map Column Text (λ(c : Column) → c.name) xs)

let optname =
        λ(n : Optional Text)
      → Optional/fold Text n Text (λ(n : Text) → n ++ " ") ""

in    λ(i : ./Type.dhall)
    →     (if i.unique then "UNIQUE " else "")
      ++  (if i.primary then "PRIMARY " else "")
      ++  "INDEX "
      ++  optname i.name
      ++  "(${collist i.columns})"
