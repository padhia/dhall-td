let Column = ../Column/Type.dhall

in  { columns :
        List Column
    , name :
        Optional Text
    , unique :
        Bool
    , primary :
        Bool
    , order :
        Optional < values : Column | hash : Column >
    }
