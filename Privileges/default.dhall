{ select  = False
, insert  = False
, update  = False
, delete  = False
, execute = False

, indexes = False
, stats   = False

, create  = ../DDLPrivileges/default.dhall
, drop    = ../DDLPrivileges/default.dhall
} : ./Type.dhall
