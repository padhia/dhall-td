let DDL/any = λ(x : ../DDLPrivileges/Type.dhall) → ../DDLPrivileges/any.dhall x

in    λ(x : ./Type.dhall)
    →     x.select
      ||  x.insert
      ||  x.update
      ||  x.delete
      ||  x.indexes
      ||  x.stats
      ||  x.execute
      ||  DDL/any x.create
      ||  DDL/any x.drop
