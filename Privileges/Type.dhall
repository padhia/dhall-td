{ select: Bool
, insert: Bool
, update: Bool
, delete: Bool
, execute: Bool

, indexes: Bool
, stats: Bool

, create: ../DDLPrivileges/Type.dhall
, drop: ../DDLPrivileges/Type.dhall
}
