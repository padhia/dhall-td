let cst = (../Prelude.dhall).Text.concatSep ", "

let sql = λ(x : ./Type.dhall) → cst (./toList.dhall x)

let example0 = assert : sql (./default.dhall ⫽ { select = True }) ≡ "SELECT"

let example1 =
        assert
      :   sql (./default.dhall ⫽ { select = True, insert = True })
        ≡ "SELECT, INSERT"

in  sql
