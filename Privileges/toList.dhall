let auth = λ(x : Bool) → λ(y : Text) → if x then [ y ] else [] : List Text

let ddl_auth =
        λ(c : Bool)
      → λ(d : Bool)
      → λ(x : Text)
      →       if c && d

        then  [ x ]

        else  if c

        then  [ "CREATE ${x}" ]

        else  if d

        then  [ "DROP ${x}" ]

        else  [] : List Text

let toList =
        λ(x : ./Type.dhall)
      →   auth x.select  "SELECT"
        # auth x.insert  "INSERT"
        # auth x.update  "UPDATE"
        # auth x.delete  "DELETE"
        # auth x.indexes "INDEXES"
        # auth x.stats   "STATS"
        # (       if x.execute

            then  [ "EXECUTE", "EXECUTE FUNCTION", "EXECUTE PROCEDURE" ]

            else  [] : List Text
          )
        # ddl_auth x.create.table       x.drop.table     "TABLE"
        # ddl_auth x.create.view        x.drop.view      "VIEW"
        # ddl_auth x.create.macro       x.drop.macro     "MACRO"
        # ddl_auth x.create.trigger     x.drop.trigger   "TRIGGER"
        # ddl_auth x.create.role        x.drop.role      "ROLE"
        # ddl_auth x.create.database    x.drop.database  "DATABASE"
        # ddl_auth x.create.user        x.drop.user      "USER"
        # ddl_auth x.create.proc        x.drop.proc      "PROCEDURE"
        # ddl_auth x.create.zone        x.drop.zone      "ZONE"
        # ddl_auth x.create.owner_proc  False            "OWNER PROCEDURE"
        # ddl_auth x.create.ext_proc    False            "EXTERNAL PROCEDURE"

let _ddl_dflt = ../DDLPrivileges/default.dhall

let example0 =
      assert : toList (./default.dhall ⫽ { select = True }) ≡ [ "SELECT" ]

let example1 =
        assert
      :   toList (./default.dhall ⫽ { select = True, insert = True })
        ≡ [ "SELECT", "INSERT" ]

let example2 =
        assert
      :   toList (./default.dhall ⫽ { create = _ddl_dflt ⫽ { table = True } })
        ≡ [ "CREATE TABLE" ]

let example3 =
        assert
      :   toList
            (   ./default.dhall
              ⫽ { create = _ddl_dflt ⫽ { table = True }
                , drop = _ddl_dflt ⫽ { table = True }
                }
            )
        ≡ [ "TABLE" ]

let example4 =
        assert
      :   toList
            (   ./default.dhall
              ⫽ { create = _ddl_dflt ⫽ { table = True, view = True }
                , drop = _ddl_dflt ⫽ { table = True }
                }
            )
        ≡ [ "TABLE", "CREATE VIEW" ]

in  toList
