let sql =
        λ(l : ./Type.dhall)
      → "DECIMAL(${Natural/show l.prec},${Natural/show l.scale})"

let example0 = assert : sql { prec = 10, scale = 3 } ≡ "DECIMAL(10,3)"

in  sql
